#version 330 core
layout (location = 0) in vec2 vertPosition;
layout (location = 1) in vec4 vertColour;

out vec4 _vertColour;

void main()
{
    _vertColour = vertColour;

    gl_Position = vec4(vertPosition, 0.0, 1.0);
}