/// When I need to quickly test out some OpenGL stuff, I set the version to 'TestingMain' to generate a main function.
version(TestingMain)
{
	import std.stdio, std.experimental.logger, std.string;
	import jengine.common.maths, jengine.common.gl, jengine.graphics.window, jengine.graphics.vertex, jengine.graphics.shader;
	import jaster.post;
	import derelict.glfw3;

	void main()
	{
		auto logger = new MultiLogger();
		logger.insertLogger("stdout", sharedLog);
		//logger.insertLogger("file",   new FileLogger("OpenGL_Test.log"));

		sharedLog = logger;

		DerelictGLFW3.load();
		DerelictGL3.load();
		auto window = new Window("OpenGL testing");
		DerelictGL3.reload();

		infof("OpenGL Version: %s", glGetString(GL_VERSION).fromStringz);

		auto onEscape = window.events.subscribe(Window.Events.keyInput, 
		(office, m)
		{
			auto mail = cast(ValueMail!(Window.KeyEvent))m;
			assert(mail !is null);

			if(mail.value.key == GLFW_KEY_ESCAPE)
				window.close();
		});
		scope(exit) window.events.unsubscribe(onEscape);

		while(true)
		{
			auto info = nextGLError();
			if(info.code == GL_NO_ERROR)
				break;

			error(info.message);
		}

		const size = window.size;
		glViewport(0, 0, size.x, size.y);
		glClearColor(0.5f, 0f, 0.5f, 1.0f);

		Vertex[] verts =
		[
			Vertex(-0.5, -0.5, Vector4f(1.0, 0.0, 0.0, 1.0)),
			Vertex(0.5,  -0.5, Vector4f(0.0, 1.0, 0.0, 1.0)),
			Vertex(0.0,   0.5, Vector4f(0.0, 0.0, 1.0, 1.0))
		];

		auto buffer = VertexBuffer.create(verts);
		auto shader = Shader(defaultVertexShader, defaultFragmentShader);
		scope(exit) shader.destroy();

		while(!window.shouldClose)
		{
			while(true)
			{
				auto info = nextGLError();
				if(info.code == GL_NO_ERROR)
					break;

				error(info.message);
			}

			glClear(GL_COLOR_BUFFER_BIT);

			shader.use();
			buffer.bind();
			glDrawArrays(GL_TRIANGLES, 0, 3);
			buffer.unbind();

			window.pollEvents();
			window.swapBuffers();
		}
	}
}