﻿module jengine.graphics.texture;

private
{
    import std.exception : assumeUnique, enforce;
    import std.math      : PI, fmax;

    import derelict.sdl2.sdl, derelict.sdl2.image;

    import jengine.core.sdl, jengine.core.allocators, jengine.core.timer;
    import jengine.graphics.renderer, jengine.graphics.camera, jengine.graphics.window;
	
	enum PI_180  = (PI / 180);
	enum _180_PI = (180 / PI);
}

/++
 + Wraps around an SDL_Texture*.
 + It's a class because I feel it's better for it to be a reference type.
 + ++/
class Texture
{
    private
    {
        SDL_Texture*    _texture;
        Colour          _colour;
    }

    public
    {
        /++
         + Creates a wrapper around the given handle.
         + 
         + Throws:
         +  $(B Exception) if $(B handle) is null.
         + 
         + Parameters:
         +  handle = The handle to wrap around.
         + ++/
        @safe
        this(SDL_Texture* handle)
        {
            enforce(handle !is null, "The handle must not be null.");
            this._texture = handle;
            this._colour  = Renderer.white;
        }

        ~this()
        {
            if(this._texture !is null)
                SDL_DestroyTexture(this._texture);
        }

        /++
         + Loads in a texture from an image, and then creates a wrapper around it.
         + The class is allocated with theAllocator. The GC is used if an exception is thrown.
         + If $(B file) does not end with a '\0', then $(B file).length+1 bytes of memory are allocated (with theAllocator) to add in this '\0'.
         + 
         + Throws:
         +  $(B Exception) if $(B file) is null or has a length of 0.
         +  $(B IMGException) if the file at $(B file) could not be loaded.
         + 
         + Parameters:
         +  path = The path to the file to load.
         + ++/
        static Texture fromFile(string file)
        {
            enforce(file !is null && file.length != 0, "The file path is either null or empty.");

            // Quickly add a null terminator if it doesn't exist.
            bool   dealloc = false;
            char[] buffer;
            if(file[$-1] != '\0')
            {
                buffer          = theAllocator.makeArray!char(file.length + 1);
                buffer[0..$-1]  = file;
                buffer[$-1]     = '\0';

                file    = buffer.assumeUnique;
                dealloc = true;
            }

            auto handle = IMG_LoadTexture(Renderer.handle, file.ptr);
            enforce(handle !is null, new IMGException("Unable to load file at path: " ~ file));

            file = null;
            if(dealloc)
                theAllocator.dispose(buffer);

            return Allocators.Texture.make!Texture(handle);
        }

        /++
         + Disposes of the given Texture, including deallocating it.
         + Only call this function on textures made with Texture.fromFile, or with Allocators.Texture.make
         + 
         + Parameters:
         +  texture = The texture to dispose of.
         +            It is set to null afterwards.
         + ++/
        static void dispose(ref Texture texture)
        {
            if(texture is null)
                return;

            Allocators.Texture.dispose(texture);
            texture = null;
        }

        /++
         + Gets the size of the texture.
         + ++/
        @property @nogc @trusted
        Vector2u size() nothrow
        {
            int x,y;
            SDL_QueryTexture(this.handle, null, null, &x, &y);

            return Vector2u(x, y);
        }

        /++
         + Set the colour modulation of the texture.
         + Note, this effects *All* references to this texture.
         + ++/
        @property @nogc @trusted
        void colour(Colour col) nothrow
        {
            SDL_SetTextureAlphaMod(this.handle, col.a);
            SDL_SetTextureColorMod(this.handle, col.r, col.g, col.b);

            this._colour = col;
        }

        /++
         + Get the colour modulation of the texture.
         + ++/
        @property @nogc @trusted
        inout(Colour) colour() nothrow pure inout
        {
            return this._colour;
        }

        /++
         + Get the handle for this texture.
         + ++/
        @property @nogc @safe
        inout(SDL_Texture*) handle() nothrow inout
        {
            return this._texture;
        }
    }
}

/++
 + Defines what kind of bounding shape is used for the sprite.
 + ++/
enum BoundingType : ubyte
{
    /// Rectangle, not recommended if the sprite is going to be rotated.
    Box,

    /// Circle,  better for sprites that are going to be rotated/non-rectangular sprites.
    Circle
}

/++
 + A simple class containing a Texture, as well as all the information needed to draw it.
 + ++/
class Sprite
{
    import std.math : atan2, sin, cos;

    private
    {
        Texture             _texture;
        double              _angle; // Degrees
        Vector2f            _position;
        Vector2i            _centre;
        Rectangle           _source;
        Colour              _colour;
        SDL_RendererFlip    _flip;
        BoundingType        _type;
        Camera              _camera;
    }

    public
    {
        /++
         + Creates a sprite around the given texture.
         + 
         + Asserts:
         +  $(B texture) must not be null.
         + 
         + Parameters:
         +  texture  = The texture the sprite uses.
         +  position = The position of the sprite.
         +  bounding = The type of bounding shape to use.
         + ++/
        @nogc @safe
        this(Texture texture, Vector2f position = Vector2f(0, 0), BoundingType bounding = BoundingType.Box) nothrow
        {
            assert(texture !is null, "Cannot make a sprite around a null texture");

            auto size       = texture.size;
            this._texture   = texture;
            this._angle     = 0.0;
            this._position  = position;
            this._centre    = Vector2i(size.x / 2, size.y / 2);
            this._source    = Rectangle(0, 0, size.x, size.y);
            this._flip      = SDL_FLIP_NONE;
            this._colour    = Renderer.white;
            this._type      = bounding;
        }

        /// Ditto
        static Sprite make(Texture texture, Vector2f position = Vector2f(0, 0), BoundingType bounding = BoundingType.Box)
        {
            return Allocators.Sprite.make!Sprite(texture, position, bounding);
        }

        /++
         + Disposes of the given Sprite, including deallocating it.
         + Only call this function with Sprites made from Sprite.make, or Allocators.Sprite.make
         + 
         + Parameters:
         +  sprite = The sprite to dispose of.  
         +           It is set to null afterwards.
         + ++/
        static void dispose(ref Sprite sprite)
        {
            if(sprite is null)
                return;

            Allocators.Sprite.dispose(sprite);
            sprite = null;
        }

        /++
         + Makes the sprite move in it's rotated direction.
         + 
         + Parameters:
         +  speed = How many pixels to move.
         + ++/
        @nogc @safe
        void moveInDirection(float speed) nothrow
        {
            auto direction = Vector2f(
                cos(this.angleAsRadians),
                sin(this.angleAsRadians)
                );
                
            direction        = direction.normalised;
            this.position   += direction * Vector2f(speed, speed);
        }

        /++
         + Checks to see if this sprite is colliding with another sprite.
         + 
         + Parameters:
         +  other = The other sprite to check.
         + 
         + Returns:
         +  $(B true) if this sprite and the $(B other) sprite are colliding.
         +  $(B false) if $(B other) is null.
         + ++/
        @nogc @safe
        bool collides(Sprite other) nothrow
        {
            if(other is null)
                return false;

            enum Box         = BoundingType.Box;
            enum Circle      = BoundingType.Circle;

            enum BoxBox      = Box       + Box;
            enum BoxCirc     = Box       + Circle;
            enum CircCirc    = Circle    + Circle;
            enum Max         = CircCirc  + 1;

            auto type = this.bounding + other.bounding;
            assert(type < Max, "Unsupported collision");

            final switch(type)
            {
                case BoxBox:
                    return this.box.intersectsWith(other.box);

                case BoxCirc:
                    auto box    = this.box;
                    auto ci     = other.circle;

                    auto a      = Vector2f(box.x,           box.y);
                    auto b      = Vector2f(box.x + box.w,   box.y);
                    auto c      = Vector2f(box.x,           box.y + box.h);
                    auto d      = Vector2f(box.x + box.w,   box.y + box.h);

                    // TODO: Make a function to see if a *line* intersects the circle, as just checking the corners will be inaccurate.
                    return (
                        box.hasPoint(ci.centre.asVector!int)  ||
                        ci.hasPoint(a)                             ||
                        ci.hasPoint(b)                             ||
                        ci.hasPoint(c)                             ||
                        ci.hasPoint(d)
                        );

                case CircCirc:
                    return this.circle.collides(other.circle);
            }
        }

        /++
         + Rotates the sprite to a given point.
         + 
         + Parameters:
         +  point = The point to rotate towards.
         + ++/
        @nogc @safe
        void rotateTo(Vector2i point) nothrow
        {
            this.angleAsRadians = atan2(point.y - (this._position.y + this.centre.y), point.x - (this._position.x + this.centre.y));
        }

        /++
         + Sets the flip mode of the sprite.
         + 
         + Asserts:
         +  $(B flip) must be SDL_FLIP_NONE, SDL_FLIP_HORIZONTAL, or SDL_FLIP_VERTICAL
         + ++/
        @property @nogc @safe
        void flip(SDL_RendererFlip flip) nothrow
        {
            assert(flip == SDL_FLIP_NONE || flip == SDL_FLIP_HORIZONTAL || flip == SDL_FLIP_VERTICAL, "Invalid flip mode was given.");

            this._flip = flip;
        }

        /++
         + Gets the flip mode of the sprite.
         + ++/
        @property @nogc @safe
        inout(SDL_RendererFlip) flip() nothrow inout
        {
            return this._flip;
        }

        /++
         + Gets a reference to the source rectangle of the sprite.
         + The source rectangle is what part of the texture is rendered.
         + ++/
        @property @nogc @safe
        ref inout(Rectangle) sourceRect() nothrow inout
        {
            return this._source;
        }

        /++
         + Gets a reference to the centre point of rotation for the sprite.
         + ++/
        @property @nogc @safe
        ref inout(Vector2i) centre() nothrow inout
        {
            return this._centre;
        }

        /++
         + Gets a reference to the position of the sprite.
         + ++/
        @property @nogc @safe
        ref inout(Vector2f) position() nothrow inout
        {
            return this._position;
        }

        /++
         + Sets the rotation of the sprite in degrees.
         + ++/
        @property @nogc @safe
        void angleAsDegrees(double degrees) nothrow
        {
            this._angle = degrees;
        }
        
        /++
         + Gets the rotation of the sprite in degrees.
         + ++/
        @property @nogc @safe
        inout(double) angleAsDegrees() nothrow inout
        {
            return this._angle;
        }

        /++
         + Sets the rotation of the sprite in radians.
         + ++/
        @property @nogc @safe
        void angleAsRadians(double radians) nothrow
        {
            this._angle = radians * _180_PI;
        }
        
        /++
         + Gets the rotation of the sprite in radians.
         + ++/
        @property @nogc @safe
        inout(double) angleAsRadians() nothrow inout
        {
            return this._angle * PI_180;
        }

        /++
         + Get the sprite's texture
         + ++/
        @property @nogc @safe
        inout(Texture) texture() nothrow inout
        out(text)
        {
            assert(text !is null, "Somehow the texture became null...");
        }
        body
        {
            return this._texture;
        }

        /++
         + Gets the circle bounding box (#Wat) of the sprite.
         + ++/
        @property @nogc @safe
        Circle circle() nothrow
        {
            auto size = this.size;
            return Circle(fmax(size.x/2, size.y/2), this._position + this._centre.asVector!float);
        }

        /++
         + Gets the rectangle bounding box of the sprite.
         + ++/
        @property @nogc @safe
        Rectangle box() nothrow
        {
            auto size = this.size;
            return Rectangle(cast(int)this.position.x, cast(int)this.position.y, size.x, size.y);
        }

        /++
         + Gets the size of the sprite.
         + The size is based off of the sprite's sourceRect, so this is preferred over using the texture's size.
         + ++/
        @property @nogc @safe
        Vector2i size() nothrow
        {
            Vector2i size;
            size.x = this.sourceRect.w;
            size.y = this.sourceRect.h;
            return size;
        }

        /++
         + Gets the bounding shape type of the sprite.
         + ++/
        @property @nogc @safe
        BoundingType bounding() nothrow inout
        {
            return this._type;
        }

        /++
         + Gets a reference to this Sprite's colour.
         + Does *Not* effect any other sprite using the same texture.
         + ++/
        @property @nogc @safe
        ref inout(Colour) colour() nothrow inout
        {
            return this._colour;
        }

        /++
         + Gets a reference to the texture's camera.
         + The camera is used to determine where the sprite should show up on screen. And in the future what extra rotation, flip flags, scale etc. it may have.
         + ++/
        @property @nogc @safe
        ref inout(Camera) camera() nothrow inout
        {
            return this._camera;
        }

        /++
         + Calculates whether the Sprite is currently on screen or not.
         + This will $(B always) be true if no camera is attched to the sprite.
         + ++/
        @property @trusted @nogc
        bool isOnScreen() nothrow
        {
            if(this.camera is null)
                return true;
            
            // Calculate whether it's on screen, and set a flag for it.
            auto pos        = this.position;
            auto size       = this.size;
            auto screen     = this.camera.box;
            
            return (
                screen.hasPoint(Vector2f(pos.x,          pos.y).asVector!int)            ||
                screen.hasPoint(Vector2f(pos.x + size.x, pos.y).asVector!int)            ||
                screen.hasPoint(Vector2f(pos.x,          pos.y + size.y).asVector!int)   ||
                screen.hasPoint(Vector2f(pos.x + size.x, pos.y + size.y).asVector!int)
                );
        }
    }
}

/++
 + Contains all the information needed for an animation to play.
 + ++/
struct AnimationInfo
{
    /++
     + How wide each frame is, in pixels.
     + ++/
    uint    frameWidth;

    /++
     + How high each frame is, in pixels.
     + ++/
    uint    frameHeight;

    /++
     + How much time, in seconds, each frame should be displayed for until moving onto the next.
     + ++/
    float   duration = 0.0f;

    /++
     + If $(D true) then each row in the animation is looped.
     + If $(D false) then the last frame of the row will be used once the animation has ended.
     + ++/
    bool    loop = false;
}

/++
 + A class that provides a way of simple animation rendering.
 + 
 + Animation frames will be played on the same "X-Row", and user-code can change which "Y-column" of animation frames to use.
 + 
 + E.G:
 + {
 +    _|_1__2__3__ > X row
 +    1| A  B  C   > X row
 +    2| X  Y  Z   > X row
 +    v  v  v  v
 +     Y column
 + }
 + 
 + Using this example, if Y-column 1 is used, then the frames "A", "B", and "C" would loop.
 + When Y-column 2 is selected, then the frames "X", "Y", "Z" are used instead.
 + 
 + The number of X-rows and Y-columns needed are calculated by the class.
 + ++/
class Animation
{
    private
    {
        Sprite          _sprite;
        AnimationInfo   _info;
        uint            _frameX, _frameY;
        uint            _maxX, _maxY;
        Timer           _timer;
        bool            _animating;

        @nogc @safe
        void nextFrame(bool nextX) nothrow
        {
            // Advance the frame
            if(nextX)
            {
                this._frameX += 1;

                if(this._frameX == this._maxX)
                {
                    if(this._info.loop)
                        this._frameX = 0;
                    else
                        this._animating = false;
                }
            }

            // Tell the sprite what frame to actually draw.
            this._sprite.sourceRect = Rectangle
                (
                    this._info.frameWidth * this._frameX, 
                    this._info.frameHeight * this._frameY,
                    this._info.frameWidth,
                    this._info.frameHeight
                );
        }
    }

    public
    {
        /++
         + Creates a new animation.
         + 
         + Asserts:
         +  The texture should be large enough to fit in at least 1 frame.
         + 
         + Parameters:
         +  texture = The texture holding all of the animation frames.
         +  info    = Information on how to render the animation.
         + ++/
        this(Texture texture, AnimationInfo info)
        {
            this._info = info;

            this._sprite        = Sprite.make(texture);
            this._sprite.centre = Vector2i(info.frameWidth / 2, info.frameHeight / 2);

            auto size  = texture.size;
            this._maxX = cast(uint)(size.x / info.frameWidth);
            this._maxY = cast(uint)(size.y / info.frameHeight);
            assert(this._maxX != 0, "The animation needs to at least fit in 1 frame.");
            assert(this._maxY != 0, "The animation needs to at least fit in 1 frame.");

            this.nextFrame(false);
        }

        /// Ditto
        static Animation make(Texture texture, AnimationInfo info)
        {
            return Allocators.Sprite.make!Animation(texture, info);
        }

        /++
         + Disposes of the given animation.
         + 
         + Parameters:
         +  animation = The animation to dispose of.
         +              It is set to null afterwards.
         + ++/
        static void dispose(ref Animation animation)
        {
            if(animation is null)
                return;

            Sprite.dispose(animation._sprite);
            Allocators.Sprite.dispose(animation);

            animation = null;
        }

        /++
         + Starts the animation
         + ++/
        @nogc @safe
        void start() nothrow
        {
            this._animating = true;
            this._frameX    = 0;
            this._timer.start();
        }

        /++
         + Stops the animation
         + ++/
        @nogc @safe 
        void stop() nothrow
        {
            this._animating = false;
        }

        /++
         + Updates the animation.
         + ++/
        @nogc @safe
        void onUpdate() nothrow
        {
            import std.math : approxEqual;

            // Update to the next frame if needed.
            if(this._animating)
            {
                auto time = this._timer.asSeconds;
                if(time > this._info.duration || time.approxEqual(this._info.duration))
                {
                    this.nextFrame(true);
                    this._timer.restart();
                }
            }
        }

        /++
         + Advances the animation to the next column.
         + Resets the column to 0 if there's no more columns left.
         + ++/
        @nogc @safe
        void nextColumn() nothrow
        {
            this.column = (this._frameY + 1);
        }

        /++
         + Sets the Y-column of the animation.
         + If the value given is too large, then the column is set to 0.
         + ++/
        @property @nogc @safe
        void column(uint y) nothrow
        {
            if(y >= this._maxY)
                y = 0;

            this._frameX = 0;
            this._frameY = y;

            this.nextFrame(false);
        }

        /++
         + Gets the sprite this animation is using.
         + This is the only real way to do things such as attaching a camera to the animation.
         + ++/
        @property @nogc @safe
        inout(Sprite) sprite() nothrow inout
        {
            return this._sprite;
        }

        /++
         + Get whether or not the animation is currently animating a row.
         + ++/
        @property @nogc @safe
        bool animating() nothrow
        {
            return this._animating;
        }

        /++
         + Gets a refernce to the bool determining if the animation should loop or not.
         + ++/
        @property @nogc @safe
        ref bool loop() nothrow
        {
            return this._info.loop;
        }
    }
}