///
module jengine.graphics.window;

public 
{
    import jengine.common.maths : Vector2i;
    import jengine.common.input : InputState, KeyModifier;
}

private
{
    import derelict.glfw3;
    import jaster.post;
}

/// Values should be from the "GLFW_KEY_XXX" enums.
alias GLFWKey = int;

/// $(B For now, this class is simply used to access a single instance of Window, until I can be bothered to add multi-window support.)
final class WindowManager
{
    static private
    {
        Window[string] _windows;
    }

    static public
    {
        ///
        @trusted @nogc
        Window get(const GLFWwindow* handle) nothrow
        {
            foreach(window; this._windows.byValue)
            {
                if(window.handle == handle)
                    return window;
            }

            return null;
        }

        ///
        @property @safe
        Window window() nothrow
        {
            auto ptr = ("main" in this._windows);
            return (ptr is null) ? null : *ptr;
        }

        ///
        @property @safe
        void window(Window wnd) nothrow
        {
            this._windows["main"] = wnd;
        }
    }
}

///
final class Window
{
    import std.experimental.logger;
    
    ///
    struct GLContext
    {
        ///
        int major;

        ///
        int minor;
    }

    private
    {
        GLFWwindow*      _handle;
        const(GLContext) _context;
        PostOffice       _events;
    }

    public
    {
        /++
         + Creates a new window.
         +
         + Notes:
         +  The window will automatically register itself with the `WindowManager`.
         +
         + Params:
         +  name    = The name of the window.
         +  size    = The size of the window.
         +  context = Information about the window's OpenGL context.
         + ++/
        this(string name, const Vector2i size = Vector2i(800, 600), const GLContext context = GLContext(3, 3))
        {
            import std.string : toStringz;

            tracef("Creating window called '%s', with size of '%s', with context of '%s'.", name, size, context);
            fatal(&glfwInit is null, "GLFW has not been loaded via Derelict yet.");

            this._context = context;

            // Initialise GLFW
            trace("Initialising GLFW.");

            auto couldInit = glfwInit();
            fatal(!couldInit, "Could not initialise GLFW");

            // Setup the context.
            trace("Setting up GLFW context.");

            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, context.major);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, context.minor);
            glfwWindowHint(GLFW_OPENGL_PROFILE,        GLFW_OPENGL_CORE_PROFILE);

            version(OSX)
                glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);

            // Setup the window
            trace("Creating GLFW window handle.");

            this._handle = glfwCreateWindow(size.x, size.y, name.toStringz, null, null);
            fatal(this._handle is null, "GLFW was unable to create it's window.");

            glfwMakeContextCurrent(this._handle);

            // Final setup
            WindowManager.window = this; // @@TEMP@@
            this._events = new PostOffice();
            this.registerCallbacks();
        }

        /++
         + Closes the window.
         + ++/
        @trusted @nogc
        void close() nothrow
        {
            glfwSetWindowShouldClose(this.handle, GLFW_TRUE);
        }

        /++
         + Swaps the active colour buffer for the inactive one, and renders the currently-active one
         + to the screen.
         + ++/
        @nogc
        void swapBuffers() nothrow
        {
            glfwSwapBuffers(this.handle);
        }

        /++
         + Processes any new events.
         + ++/
        @nogc
        void pollEvents() nothrow
        {
            glfwPollEvents();
        }

        /++
         + Returns:
         +  Whether the window should close.
         + ++/
        @property @trusted @nogc
        bool shouldClose() nothrow
        {
            return glfwWindowShouldClose(this.handle) == GLFW_TRUE;
        }

        /++
         + Returns:
         +  The `GLContext` that was passed to the constructor.
         + ++/
        @property @safe @nogc
        const(GLContext) context() nothrow pure const
        {
            return this._context;
        }

        /++
         + Returns:
         +  The window's handle.
         + ++/
        @property @safe @nogc
        inout(GLFWwindow*) handle() nothrow pure inout
        out(ptr)
        {
            assert(ptr !is null);
        }
        body
        {
            return this._handle;
        }

        /++
         + Returns:
         +  The size of the window.
         + ++/
        @property @safe @nogc
        Vector2i size() nothrow
        {
            Vector2i vect;
            ()@trusted
            {
                glfwGetWindowSize(this._handle, &vect.x, &vect.y);
            }();

            return vect;
        }

        /++
         + Returns:
         +  A `PostOffice` that fires all of the window's events.
         + ++/
        @property @safe @nogc
        PostOffice events() nothrow pure
        out(office)
        {
            assert(office !is null);
        }
        body
        {
            return this._events;
        }
    }

    /++++++++++++ Everything to do with callbacks go here. +++++++++++/

    /// The different types of events that can fire.
    enum Events : Mail.MailTypeT
    {
        /// Fired whenever a key changes state.
        /// This event fires a `ValueMail!KeyEvent`
        keyInput
    }

    /// Contains information about a key being pressed/released/repeated.
    struct KeyEvent
    {
        /// The key that triggered the event.
        GLFWKey key;

        /// The scancode of the key.
        int scancode;

        /// What action caused this key to trigger the event.
        InputState action;

        /// What modifiers were pressed down when this event was fired.
        KeyModifier modifiers;
    }

    private
    {
        void registerCallbacks()
        {
            trace("Registering callbacks");
            glfwSetKeyCallback(this.handle, &keyCallback);
        }

        static
        {
            extern(C) void keyCallback(GLFWwindow* handle, GLFWKey key, int scancode, int action, int mods) nothrow
            {
                handle.doCallback((window)
                {
                    window.events.mailValue(Events.keyInput, 
                        KeyEvent(
                            key,
                            scancode,
                            cast(InputState)action,
                            cast(KeyModifier)mods
                        ));
                });
            }
        }
    }
}

private void doCallback(GLFWwindow* handle, scope void delegate(Window) callback) nothrow
{
    auto window = WindowManager.get(handle);
    assert(window !is null, "A window wasn't registered");

    try
    {
        callback(window);
    }
    catch(Exception ex)
    {
    }
}