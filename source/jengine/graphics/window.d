﻿module jengine.graphics.window;

private
{
    import std.string       : toStringz;
    import std.exception    : enforce;

    import derelict.sdl2.sdl;

    import jengine.core.sdl, jengine.graphics.renderer, jengine.core.component, jengine.core.timer, jengine.core.gamestate,
        jengine.core.allocators;
}

/++
 + Describes FPS of things.
 + ++/
struct FPS
{
    /// The desired FPS
    uint fps;

    /// How much time a frame such take, max.
    uint timeStep;

    /++
     + Calculates the timeStep from the given FPS.
     + ++/
    this(uint fps)
    {
        this.fps = fps;
        this.timeStep = (1000 / fps);
    }
}

/++
 + This class provides access to the game's main window.
 + (Don't really want to bother supporting multiple windows)
 + ++/
static class Window
{
    private static
    {
        GameStateManager _manager;
        SDL_Window*      _handle;
        FPS              _fps;
    }

    public static
    {
         /++
         + Creates the window if it hasn't been created already.
         + 
         + Description:
         +  If the window already exists, this function does nothing.
         + 
         +  This function will also call Renderer.create
         + 
         + Parameters:
         +  title   = The title of the window.
         +  window  = The size and position of the window.
		 +  		  Use SDL_WINDOWPOS_CENTERED for the x and y position for a centered window.
         +  fps     = The Desired FPS of the window.
         +  flags   = The window's flags. SDL_WINDOW_XXX
         + 
         + Throws:
         +  $(B SDLException) if the window could not be created.
         + ++/
        @trusted
        void create(string title, const(Rectangle) window, FPS fps, const(uint) flags = SDL_WINDOW_SHOWN)
        {
            if(Window._handle !is null)
                return;

            this._fps = fps;
            this._handle = SDL_CreateWindow(
                    title.toStringz,
                    window.x, window.y,
                    window.w, window.h,
                    flags);
            enforce(this._handle !is null, new SDLException("Unable to create window"));

            Renderer.create(SDL_CreateRenderer(this._handle, -1, SDL_RENDERER_ACCELERATED));

            this._manager = Allocators.GameState.make!GameStateManager();
        }

        /++
         + Destroys the window.
         + Does nothing if the window isn't open.
         + This function will also call Renderer.destroy
         + ++/
        void destroy()
        {
            if(this._handle !is null)
            {
                Renderer.destroy();

                SDL_DestroyWindow(this.handle);
                this._handle = null;

                this._manager.onUninit();
                Allocators.GameState.dispose(this._manager);
            }
        }

        /++
         + Begins the game loop.
         + 
         + Parameters:
         +  mainState = The main GameState that runs the game.
         +              This GameState should *not* be popped off of the manager.
         +              Must be allocated with an allocator compatible with Allocators.Default
         +              Do *not* delloacte this object yourself. The Window's GameStateManager will handle that.
         + ++/
        void startGameLoop(GameState mainState)
        {
            auto id = this._manager.register(mainState);
            this._manager.push(id);

            // Window loop
            SDL_Event e;
            Timer     clock;
            auto      delta = 1.0f; // 1, so we don't divide by 0, or have issues with NaN

            bool running = true;
            while(running)
            {
                // Handle events.
                while(SDL_PollEvent(&e))
                {
                    if(e.type == SDL_QUIT)
                        running = false;

                    InputManager.onEvent(e);
                    this._manager.onEvent(e, false);
                }
                
                // Clear the screen, update the game, and then swap the buffers
                Renderer.clear(Renderer.white);
                this._manager.onUpdate(delta);
                Renderer.present();

                InputManager.onUpdate();

                // Limit the frame rate
                auto time = clock.asMilliseconds;
                if(time < this._fps.timeStep)
                    SDL_Delay(this._fps.timeStep - time);
                
                delta = clock.asSeconds;
                clock.restart();
            }
        }

        /++
        + Set the desired FPS for the window.
        + ++/
        @property @nogc @safe
        void fps(FPS fps_) nothrow
        {
            this._fps = fps_;
        }

        /++
         + Get the size of the window.
         + ++/
        @property @nogc @trusted
        Vector2i size() nothrow
        {
            Vector2i toReturn;
            SDL_GetWindowSize(this.handle, &toReturn.x, &toReturn.y);

            return toReturn;
        }

        /++
         + Get the window's SDL_Window handle.
         + 
         + Asserts:
         +  The handle must not be null.
         + ++/
        @property @nogc @safe
        SDL_Window* handle() nothrow
        {
            assert(this._handle !is null, "The handle must not be null. Call Window.create please.");
            return Window._handle;
        }
    }
}

/++
 + Manages input from the game window.
 + ++/
static class InputManager
{
    enum InputBufferLength = 512;

    /// Contains information about a key's state
    struct KeyInfo
    {
        /// Is the key currently being pressed down?
        bool    down;

        /// Was the key pressed down this frame?
        bool    pressed;
    }

    private static
    {
        // Any non-hand made SDL_Events should work fine, so I'm not bothering with bounds checking.
        KeyInfo[SDL_NUM_SCANCODES]  _codes;
        bool                        _fixCodes;

        // Stores the text of SDL_TEXTINPUT
        char[InputBufferLength]     _inputBuffer;
        size_t                      _index;

        @nogc @trusted
        void bufferText(char* text) nothrow
        {
            import std.c.string : strlen;

            auto length =  strlen(text);
            auto before =  this._index;
            this._index += length;

            // Keeps the index in bounds
            if(this._index > InputBufferLength)
                this._index = InputBufferLength;

            this._inputBuffer[before..this._index] = text[0..this._index - before];
        }
    }

    public static
    {
        /++
         + Called when an event should be processed.
         + 
         + Noteable behavior:
         +  - CTRL + V support is also handled automatically
         +  - If the text buffer is full (For example, pasting in a giant block of text) then the manager will simply drop any characters it can't store.
         + 
         + Parameters:  
         +  e = The event to process.
         + ++/
        @nogc @trusted
        void onEvent(SDL_Event e) nothrow
        {
            import std.stdio;

            switch(e.type)
            {
                case SDL_KEYDOWN:
                    auto code    = &this._codes[e.key.keysym.scancode];
                    code.pressed = (!code.down);
                    code.down    = true;

                    this._fixCodes = (this._fixCodes || code.pressed);

                    // CTRL + V support. To stop the game from being overwhelmed, isKeyPressed is used instead of isKeyDown.
                    if(InputManager.isKeyPressed(SDL_SCANCODE_V) && (SDL_GetModState() & KMOD_CTRL))
                    {
                        auto text = SDL_GetClipboardText();
                        InputManager.bufferText(text);
                    }
                    break;

                case SDL_KEYUP:
                    this._codes[e.key.keysym.scancode] = KeyInfo(false, false);
                    break;

                case SDL_TEXTINPUT:
                    InputManager.bufferText(e.text.text.ptr);
                    break;

                default:
                    break;
            }
        }

        /++
         + Allows the InputManager to update itself.
         + 
         + Description:
         +  - It will clear the InputManager.textBuffer
         +  - It will make sure keycodes aren't being seen as "pressed" anymore
         + ++/
        @nogc @trusted
        void onUpdate() nothrow
        {
            this._index = 0;

            // Don't waste time going over it if we don't need to.
            if(this._fixCodes)
            {
                foreach(ref k; this._codes)
                    k.pressed = false;
            }
        }

        /++
         + Determines if a key is currently pressed down.
         + 
         + Parameters:
         +  code = The scancode of the key.
         + 
         + Returns:
         +  $(D true) if $(B code) is currently down.
         + ++/
        @nogc @safe
        bool isKeyDown(SDL_Scancode code) nothrow
        {
            return this._codes[code].down;
        }

        /++
         + Determines if a key was *just* pressed down this frame.
         + 
         + Parameters:
         +  code = The scancode of the key.
         + 
         + Returns:
         + $(D true) if $(B code) was only just pressed down.
         + ++/
        @nogc @safe
        bool isKeyPressed(SDL_Scancode code) nothrow
        {
            return this._codes[code].pressed;
        }

        /++
         + Clears the state of the InputManager.
         + This does things such as marking all keys as not pressed.
         + ++/
        @nogc @safe
        void clearState() nothrow
        {
            foreach(ref b; this._codes)
                b = KeyInfo(false, false);

            this._inputBuffer[] = '\0';
            this._index = 0;
        }

        /++
         + Gets the buffered text of this frame.
         + This includes any text entered into the window, including CTRL + V operations.
         + ++/
        @property @nogc @safe
        char[] textBuffer() nothrow
        {
            return InputManager._inputBuffer[0..this._index];
        }

        /++
         + Gets the position of the mouse.
         + ++/
        @property @nogc @trusted
        Vector2i mousePosition() nothrow
        {
            Vector2i toReturn;
            SDL_GetMouseState(&toReturn.x, &toReturn.y);

            return toReturn;
        }
    }
}