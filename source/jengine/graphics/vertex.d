///
module jengine.graphics.vertex;

public import jengine.common.maths : Vector2f, Vector4f;

private
{
    import jengine.common.gl;
}

/// Defines a 2D vertex.
struct Vertex
{
    /// The position of the vertex.
    Vector2f position;

    /// The vertex's colour.
    Vector4f colour;

    ///
    @safe @nogc
    this(Vector2f pos, Vector4f colour = Vector4f(1.0, 1.0, 1.0, 1.0)) nothrow pure
    {
        this.position = pos;
        this.colour   = colour;
    }

    ///
    @safe @nogc
    this(float x, float y, Vector4f colour = Vector4f(1.0, 1.0, 1.0, 1.0)) nothrow pure
    {
        this(Vector2f(x, y), colour);
    }
}

/++
 + A buffer of verticies.
 +
 + Usage:
 +  Bind and unbind the buffer using `VertexBuffer.bind` and `VertexBuffer.unbind`.
 + 
 +  To change the buffer's data, modify the `VertexBuffer.verticies` array, and then call
 +  `VertexBuffer.update`.
 + ++/
struct VertexBuffer
{
    private
    {
        uint  _vbo;
        uint  _vao;
    }

    public
    {
        /++
        + The verticies making up the buffer.
        +
        + $(RED Anytime this array is modified, a call to `VertexBuffer.update` must be made.)
        + ++/
        Vertex[] verticies;

        @disable
        this();

        /++
         + Updates the buffer's data to reflect the current state of `VertexBuffer.verticies`.
         +
         + Notes:
         +  This function binds this buffer as the GL_ARRAY_BUFFER.
         + ++/
        void update()
        {
            glBindBuffer(GL_ARRAY_BUFFER, this._vbo);
            glBufferData(GL_ARRAY_BUFFER, Vertex.sizeof * this.verticies.length, this.verticies.ptr, GL_DYNAMIC_DRAW);
        }

        /++
         + Binds this buffer, so any operation used on the GL_ARRAY_BUFFER is used on this buffer.
         + ++/
        void bind()
        {
            glBindVertexArray(this._vao);
        }

        /++
         + Unbinds this buffer. It is not neccessary to call this function, but can save headaches if used.
         + ++/
        void unbind()
        {
            glBindVertexArray(0);
        }

        /++
         + Creates a new `VertexBuffer`.
         +
         + This function exists so simply declaring a `VertexBuffer` wouldn't cause a VBO to generate.
         +
         + Params:
         +  verticies = The intial set of verticies to give the buffer.
         +
         + Returns:
         +  The newly created buffer.
         + ++/
        static VertexBuffer create(Vertex[] verticies = null)
        {
            auto buffer = VertexBuffer.init;
            buffer.verticies = verticies;

            // Create the buffers
            glGenVertexArrays(1, &buffer._vao);
            glGenBuffers(1, &buffer._vbo);

            // Bind the buffers
            glBindVertexArray(buffer._vao);
            glBindBuffer(GL_ARRAY_BUFFER, buffer._vbo);

            // Setup the attributes
            glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, Vertex.sizeof, null);
            glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, Vertex.sizeof, cast(void*)(2 * float.sizeof));

            foreach(i; 0..2)
                glEnableVertexAttribArray(i);

            // Unbind the buffers
            glBindVertexArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, 0);

            if(verticies !is null)
                buffer.update();

            return buffer;
        }
    }
}