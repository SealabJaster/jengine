﻿module jengine.graphics.renderer;

private
{
    import std.exception : enforce;

    import derelict.sdl2.sdl, derelict.sdl2.ttf;

    import jengine.graphics.window, jengine.core.sdl, jengine.graphics.texture, jengine.graphics.font;
}

/++
 + A wrapper around an SDL_Renderer, that also supports easily drawing any of the Engine's graphical types.
 + ++/
static class Renderer
{
    private static
    {
        SDL_Renderer*   _handle;
    }

    public static
    {
        // Some predefined colours
        immutable
        {
            auto black      = Colour(0,    0,      0);
            auto white      = Colour(255,  255,  255);
            auto gray       = Colour(128,  128,  128);
            auto blue       = Colour(0,    0,    255);
            auto green      = Colour(0,    255,    0);
            auto red        = Colour(255,  0,      0);
            auto yellow     = Colour(255,  255,    0);
            auto purple     = Colour(255,  0,    255);
            auto cyan       = Colour(0,    255,  255);
            
            alias grey = gray;
        }

        /++
         + Setup the Renderer. Does nothing if the Renderer has already been setup.
         + 
         + Parameters:
         +  handle = The SDL_Renderer* to use.
         + 
         + Throws:
         +  $(B SDLException) if the handle is null.
         + ++/
        @trusted
        void create(SDL_Renderer* handle)
        {
            if(this._handle !is null)
                return;

            enforce(handle !is null, new SDLException("Cannot use a null handle."));
            this._handle    = handle;
            this.drawColour = Renderer.white;
        }

        /++
         + Clears the screen.
         + 
         + Parameters:
         +  clearColour = The colour to clear the screen with.
         + ++/
        @trusted @nogc
        void clear(Colour clearColour) nothrow
        {
            Renderer.drawColour = clearColour;
            SDL_RenderClear(this.handle);
        }

        /++
         + Presents any changes made.
         + ++/
        @trusted @nogc
        void present() nothrow
        {
            SDL_RenderPresent(this.handle);
        }

        /++
         + Destroys the renderer. Allows Renderer.create to be called again.
         + ++/
        @nogc @trusted
        void destroy() nothrow
        {
            if(this._handle !is null)
            {
                SDL_DestroyRenderer(this._handle);
                this._handle = null;
            }
        }

        /++
         + Draws a rectangle with a border.
         + 
         + Parameters:
         +  rect            = The rectangle to draw.
         +  colour          = The colour of the rectangle.
         +  borderSize      = In pixels, how long each edge of the border is.
         +  borderColour    = The colour of the border.
         + ++/
        @nogc @trusted
        void drawRect(const(Rectangle) rect,      const(Colour) colour       = Renderer.drawColour, 
                      const(uint) borderSize = 2, const(Colour) borderColour = Renderer.black) nothrow
        {
            // Draw the border first
            if(borderSize > 0)
            {
                const border = rect + Rectangle(-borderSize, -borderSize, borderSize*2, borderSize*2);
                this.drawColour = borderColour;
                SDL_RenderFillRect(this.handle, border.handlePointer);
            }

            this.drawColour = colour;
            SDL_RenderFillRect(this.handle, rect.handlePointer);
        }

        /// Ditto
        @nogc @trusted
        void drawRect(const(RectangleSprite) rect) nothrow
        {
            Renderer.drawRect(rect.rect, rect.fillColour, rect.borderSize, rect.borderColour);
        }

        /++
         + Draws a circle. (Warning, this is slowish and sort of inaccurate. Just use it for debugging or something.)
         + 
         + Parameters:
         +  circle = The circle to draw.
         +  colour = The colour to use when drawing it.
         + ++/
        @nogc @trusted
        void drawCircle(const(Circle) circle, const(Colour) colour = Renderer.drawColour) nothrow
        {
            // Too tired to do proper mathy things, so a bunch of for loops will work...
            auto tempColour     = Renderer.drawColour;
            Renderer.drawColour = colour;

            // Variables
            auto radius         = cast(int)circle.radius;
            Vector2i pos        = Vector2i(cast(int)circle.position.x, cast(int)circle.position.y);
            Vector2i top        = pos - Vector2i(0,      radius);
            Vector2i bottom     = pos + Vector2i(0,      radius);
            Vector2i left       = pos - Vector2i(radius, 0);
            Vector2i right      = pos + Vector2i(radius, 0);

            // Cross lines
            SDL_RenderDrawLine(this.handle, top.x,  top.y,  bottom.x, bottom.y);
            SDL_RenderDrawLine(this.handle, left.x, left.y, right.x,  right.y);

            // Draw outline
            auto x = radius;
            auto y = 0;
            auto d = 1 - x;

            while(y <= x)
            {
                SDL_RenderDrawPoint(this.handle,  x + pos.x,   y + pos.y);
                SDL_RenderDrawPoint(this.handle,  y + pos.x,   x + pos.y);
                SDL_RenderDrawPoint(this.handle, -x + pos.x,   y + pos.y);
                SDL_RenderDrawPoint(this.handle, -y + pos.x,   x + pos.y);
                SDL_RenderDrawPoint(this.handle, -x + pos.x,  -y + pos.y);
                SDL_RenderDrawPoint(this.handle, -y + pos.x,  -x + pos.y);
                SDL_RenderDrawPoint(this.handle,  x + pos.x,  -y + pos.y);
                SDL_RenderDrawPoint(this.handle,  y + pos.x,  -x + pos.y);

                y += 1;

                if(d <= 0)
                    d += 2 * y + 1;
                else
                {
                    x -= 1;
                    d  = 2 * (y - x) + 1;
                }
            }

            Renderer.drawColour = tempColour;
        }

        /++
         + Draws a texture.
         +
         + Asserts:
         +  $(B texture) must not be null.
         + 
         + Parameters:
         +  texture     = The texture to render.
         +  source      = What part of the texture to render. (0, 0) for all of it.
         +  dest        = What part of the screen to draw to. (0, 0) for the entire screen.
         +  angle       = The angle in degrees of where how rotated the texture should be.
         +  centre      = The centre point of the texture. (int.max, int.max) for the very middle of the texture.
         +  flip        = An SDL_FLIP_XXX flag.
         + ++/
        @trusted @nogc
        void drawTexture(Texture texture,           const(Rectangle) source,    const(Rectangle) dest, 
                         const(double) angle = 0,   const(Vector2i)  centre = Vector2i(int.max, int.max),
                         const(SDL_RendererFlip) flip = SDL_FLIP_NONE) nothrow
        {
            assert(texture !is null, "Cannot render a null texture.");

            auto point = centre.asPoint;
            SDL_RenderCopyEx(
                Renderer.handle,
                texture.handle,
                (source == Rectangle(0, 0, 0, 0)) ? null : source.handlePointer,
                (dest == Rectangle(0, 0, 0, 0))   ? null : dest.handlePointer,
                angle,
                (centre == Vector2i(int.max, int.max)) ? null : &point,
                flip
                );
        }

        /++
         + Draws a sprite.
         + 
         + If the Sprite has a camera attached to it, but the Sprite is not in view of the camera, then the sprite is not drawn.
         + 
         + Asserts:
         +  See $(B Renderer.drawTexture)
         + 
         + Parameters:
         +  sprite = The sprite to draw.
         + ++/
        @trusted @nogc
        void drawSprite(Sprite sprite) nothrow
        {
            // Checke if it's on screen.
            bool onScreen = true;
            if(sprite.camera !is null)
                onScreen = sprite.isOnScreen;

            // Don't render sprite off screen that have a camera
            if(!onScreen)
                return;

            // Apply the camera's transformations
            auto oldPos = sprite.position;
            if(sprite.camera !is null)
            {
                sprite.position = (sprite.position - sprite.camera.position);
            }

            // Draw the sprite
            auto temp = sprite.texture.colour;
            auto size = sprite.size;
            sprite.texture.colour = sprite.colour;
            Renderer.drawTexture(
                sprite.texture,
                sprite.sourceRect,
                Rectangle(cast(int)sprite.position.x, cast(int)sprite.position.y, size.x, size.y),
                sprite.angleAsDegrees,
                sprite.centre,
                sprite.flip
                );

            sprite.texture.colour = temp;

            // Draws the bounding shape of the sprite
            debug 
            {
                if(sprite.bounding == BoundingType.Circle)
                    Renderer.drawCircle(sprite.circle, Renderer.black);
                else if(sprite.bounding == BoundingType.Box)
                {
                    temp                = Renderer.drawColour;

                    Renderer.drawColour = Renderer.black;
                    SDL_RenderDrawRect(Renderer.handle, sprite.box.handlePointer);
                    Renderer.drawColour = temp;
                }
            }

            // Unapply the transformations
            if(sprite.camera !is null)
            {
                sprite.position = oldPos;
            }
        }

        // Same rules as drawSprite.
        /// Ditto
        @nogc @trusted 
        void drawAnimation(Animation ani) nothrow
        {
            Renderer.drawSprite(ani.sprite);
        }

        /++
         + Renders text to the screen.
         + 
         + Parameters:
         +  text = The text to render.
         + ++/
        @nogc @trusted
        void drawText(Text text) nothrow
        {
            auto size = text.texture.size;
            Renderer.drawTexture(
                text.texture,
                Rectangle(0, 0, size.x, size.y),
                Rectangle(cast(int)text.position.x, cast(int)text.position.y, size.x, size.y)
                );
        }

        /++
         + Sets the renderer's draw colour
         + ++/
        @property @nogc @trusted
        void drawColour(Colour colour_) nothrow
        {
            SDL_SetRenderDrawColor(this.handle, colour_.r, colour_.g, colour_.b, colour_.a);
        }

        /++
         + Get the Renderer's draw colour.
         + ++/
        @property @nogc @trusted
        Colour drawColour() nothrow
        {
            Colour col;
            SDL_GetRenderDrawColor(this.handle, &col.r, &col.g, &col.b, &col.a);

            return col;
        }

        /++
         + Get the SDL_Renderer handle for the Renderer.
         + 
         + Asserts:
         +  The handle must not be null.
         + ++/
        @property @nogc @safe
        SDL_Renderer* handle() nothrow 
        {
            assert(this._handle !is null, "The handle is null. Please call Renderer.create (or Window.create) first.");
            return this._handle;
        }
    }
}

/++
 + A simple struct around a Rectangle, that provides any information needed to draw one.
 + ++/
struct RectangleSprite
{
    /// The rectangle to draw on screen.
    Rectangle   rect;

    /// The colour used inside the rectangle.
    Colour      fillColour;

    /// The size, in pixels, of each edge of the rectangle
    uint        borderSize      = 2;

    /// The colour of the rectangle's border.
    Colour      borderColour    = Renderer.black;
}