﻿module jengine.graphics.cache;

private
{
    import std.path;
    import std.file         : getcwd;
    import std.exception    : enforce; // I see not being able to load assets as being recoverable.

    import jengine.graphics.texture, jengine.graphics.font;
}

/++
 + A class that is able to load in and cache any graphical assets for the engine.
 + 
 + TODO: Find a hashmap that uses std.experimental.allocator so the GC can be avoided.
 + ++/
static class Cache
{
    private static
    {
        Texture[string] _textures;
        Font[string]    _fonts;
    }

    public static
    {
        /++
         + Loads a texture from the given path.
         + 
         + If the texture isn't cached, then it'll be cached.
         + If the texture is cached, the cached version will be returned.
         + 
         + Exceptions:
         +  $(B AssetLoadingException) if $(B path) cannot be loaded as a $(B Texture).
         + 
         + Parameters:
         +  path = The path to the file.
         +         The path is converted into a full path.
         + 
         + Returns:
         +  The texture at $(B path).
         + ++/
        Texture loadTexture(string path)
        {
            path = buildNormalizedPath(path.absolutePath);
            
            auto pointer = (path in this._textures);
            if(pointer is null)
            {
                auto texture         = Texture.fromFile(path);
                this._textures[path] = texture;
                
                return texture;
            }
            else
                return *pointer;
        }

        /++
         + Loads a font from the given path.
         + 
         + If the font isn't cached, then it'll be cached.
         + If the font is cached, the cached version will be returned.
         + 
         + Exceptions:
         +  $(B AssetLoadingException) if $(B path) cannot be loaded as a $(B Font).
         + 
         + Parameters:
         +  path = The path to the file.
         +         The path is converted into a full path.
         + 
         +  size = The size the font should be.
         +         Only used if the font wasn't cached.
         + 
         + Returns:
         +  The font at $(B path).
         + ++/
        Font loadFont(string path, int size)
        {
            path = buildNormalizedPath(path.absolutePath);
            
            auto pointer = (path in this._fonts);
            if(pointer is null)
            {
                auto font         = Font.fromFile(path, size);
                this._fonts[path] = font;
                
                return font;
            }
            else
                return *pointer;
        }

        /++
         + Destroys all of the assets in the cache.
         + ++/
        void dispose()
        {
            foreach(ref text; this._textures.byValue)
                Texture.dispose(text);

            foreach(ref font; this._fonts.byValue)
                Font.dispose(font);
        }

        ~this()
        {
            dispose();
        }
    }
}

/++
 + Thrown whenever an issue with loading an asset goes wrong.
 + ++/
class AssetLoadingException : Exception
{
    this(T)(string message, string file)
    {
        import std.format : format;
        super(format("Error loading %s from file %s: %s", T, file, message));
    }
}