﻿module jengine.core.gamestate;

private
{
    import jengine.core.component, jengine.core.allocators;
}

/++
 + Flags for GameState.onSwap
 + ++/
enum SwapAction
{
    /// Pushed onto the top of the stack
    Push,

    /// Popped off of the top of the stack
    Pop,

    /// Is on the top of the stack, but another GameState is pushed on top of it.
    Dethroned,

    /// The state on top of this one has been popped off of the stack. So this state is now on top.
    Throned
}

/++
 + The base class for all GameStates
 + ++/
class GameState : Component
{
    private
    {
        GameStateManager    _manager;
        ushort              _id = ushort.max;
    }

    public
    {
        abstract
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it. (Dethroned)
             +  Is put onto the top of the stack because another state was popped off. (Throned)
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action);
        }

        /++
         + Sets the manager for this gamestate.
         + ++/
        @property @nogc @safe
        void manager(GameStateManager man) nothrow
        {
            assert(this._manager is null && man !is null);
            this._manager = man;
        }

        /++
         + Gets the manager for this gamestate.
         + ++/
        @property @nogc @safe
        GameStateManager manager() nothrow pure
        {
            return this._manager;
        }

        /++
         + Sets the ID of the GameState.
         + This can only be called once.
         + ++/
        @property @nogc @safe
        void id(ushort i) nothrow
        {
            assert(this._id == ushort.max, "Please only call this function *once*. In fact, you shouldn't even be calling it anyway. The engine should.");
            this._id = i;
        }

        /++
         + Gets the ID of the GameState.
         + The ID is the ID used in it's GameStateManager.
         + ++/
        @property @nogc @safe
        ushort id() nothrow const pure
        {
            return this._id;
        }
    }
}

/++
 + A class to manage different gamestates.
 + 
 + Description:
 +  The GameStateManager is basically a stack of GameStates.
 +  You can push GameStates onto the top of the stack, and then also pop them off.
 +  The top-most GameState is the one that will be used in the onUpdate and onEvent functions.
 +  
 +  When you register a GameState, it will be given an ID, which can be used to push it onto the top of the stack.
 + ++/
class GameStateManager
{
    private
    {
        GameState[]     _registered;
        GameState[128]  _stack;
        size_t          _index;
        size_t          _registerIndex;

        IAllocator      _alloc;
    }

    public
    {
        /++
         + Constructs a new GameStateManager.
         + ++/
        this()
        {
            this._alloc = Allocators.GameState;
        }

        /++
         + Registers a new GameState into the GameStateManager.
         + Initialises the GameState if it hasn't been done already.
         + 
         + Asserts:
         +  $(B state).hasInit must be true, either when given to the function or after a call to onInit.
         +  $(B state) must not alraedy exist in the manager.
         + 
         + Parameters:
         +  state   = The GameState to register.
         +  id      = The ID to give the GameState.
         +            If the ID is ushort.max, then an ID is given to it by the manager.
         + 
         + Returns:
         +  The ID of the GameState.
         + ++/
        ushort register(GameState state, ushort id = ushort.max)
        {
            assert(state !is null, "Cannot register a null GameState");
            state.manager = this;

            if(!state.hasInit)
                state.onInit();
            assert(state.hasInit);

            import std.algorithm : filter;
            assert(this._registered.filter!((gs) => gs == state).empty, "Cannot register a GameState more than once");

            state.id = cast(ushort)((id == ushort.max) ? this._registered.length : id);

            if(this._registerIndex >= this._registered.length)
                assert(this._alloc.expandArray(this._registered, 5), "Unable to allocate memory");

            this._registered[this._registerIndex++] = state;
            return state.id;
        }

        /++
         + Pushes a state onto the stack.
         + 
         + Asserts:
         +  The stack must not be full.
         +  There must be a GameState that has the ID of $(B id)
         + 
         + Parameters:
         +  id = The ID of the GameState to push.
         + ++/
        void push(ushort id)
        {
            import std.algorithm    : filter;
            import std.conv         : to;

            auto filt = this._registered.filter!((gs) => gs.id == id);

            assert(!filt.empty, "No state with the ID of " ~ id.to!string ~ " exists.");
            assert(this._index != this._stack.length, "The GameStateManager's stack is full.");

            if(this._index != 0)
                this.current.onSwap(SwapAction.Dethroned);

            this._stack[this._index++] = filt.front;
            filt.front.onSwap(SwapAction.Push);
        }

        /++
         + Pops the current state off of the stack.
         + 
         + Asserts:
         +  The stack must not be empty.
         + ++/
        void pop()
        {
            assert(this._index != 0, "The GameStateManager's stack is empty");

            this._index -= 1;
            this._stack[this._index].onSwap(SwapAction.Pop);

            if(this._index != 0)
                this._stack[this._index-1].onSwap(SwapAction.Throned);
        }

        /++
         + Called everytime the top-most gamestate should be updated.
         + 
         + Parameters:
         +  gameTime = A reference to the amount of time the last frame took.
         + ++/
        void onUpdate(ref float gameTime)
        {
            this.current.onUpdate(gameTime);
        }
        
        /++
         + Called everytime an event is to be processed.
         + 
         + Parameters:
         +  event   = The event to process.
         +  handled = Has the event already been handled?
         + 
         + Returns:
         +  $(D true) if $(B event) was handled. $(D false) otherwise.
         + ++/
        bool onEvent(SDL_Event event, bool handled)
        {
            return this.current.onEvent(event, handled) || handled;
        }

        /++
         + Uninitialises all registered GameStates, and the manager itself
         + ++/
        void onUninit()
        {
            if(this._registered is null)
                return;

            foreach(gs; this._registered)
            {
                if(gs is null)
                    continue;
                    
                gs.onUninit();
                this._alloc.dispose(gs);
            }

            this._alloc.dispose(this._registered);

            this._index         = 0;
            this._registerIndex = 0;
        }

        /++
         + Gets the GameState that is at the top of the stack.
         + 
         + Asserts:
         +  The stack must not be empty.
         + ++/
        @property @nogc @safe
        GameState current() nothrow pure
        {
            assert(this._index > 0, "The GameStateManager's stack is empty");
            return this._stack[this._index - 1];
        }

        /++
         + Gets the GameState that is at the top of the stack.
         + ++/
        @property @nogc @safe
        T currentAs(T : GameState)() nothrow pure
        {
            return cast(T)this.current;
        }
    }
}
unittest
{
    class Dummy : GameState
    {
        public
        {
            int oi, oup, oe, ospu, ospo, osde, osth;

            override
            {
                void onInit()
                {
                    super.onInit();
                    this.oi += 1;
                }

                void onUpdate(ref float gameTime)
                {
                    this.oup += 1;
                }

                bool onEvent(SDL_Event event, bool handled)
                {
                    this.oe += 1;

                    return handled;
                }

                void onSwap(SwapAction action)
                {
                    if(action == SwapAction.Pop)
                        this.ospo += 1;
                    else if(action == SwapAction.Push)
                        this.ospu += 1;
                    else if(action == SwapAction.Throned)
                        this.osth += 1;
                    else
                        this.osde += 1;
                }

                void onUninit()
                {
                    super.onUninit();
                }
            }
        }
    }

    Allocators.setup();
    theAllocator = Allocators.GameState;

    GameStateManager manager = Allocators.GameState.make!GameStateManager();
    manager.register(theAllocator.make!Dummy, 0);
    manager.register(theAllocator.make!Dummy, 1);

    float f = 0;
    Dummy one, two;

    manager.push(0);
    one = manager.currentAs!Dummy;

    manager.push(1);
    two = manager.currentAs!Dummy;

    manager.onUpdate(f);
    manager.pop();
    manager.onEvent(SDL_Event(), false);

    assert(one.oi == 1);
    assert(two.oi == 1);

    assert(one.oup == 0);
    assert(two.oup == 1);

    assert(one.oe == 1);
    assert(two.oe == 0);

    assert(one.ospu == 1);
    assert(two.ospu == 1);

    assert(one.ospo == 0);
    assert(two.ospo == 1);

    assert(one.osde == 1);
    assert(two.osde == 0);

    assert(one.osth == 1);
    assert(two.osth == 0);

    manager.onUninit();
    Allocators.GameState.dispose(manager);
}