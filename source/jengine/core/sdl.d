﻿/++
 + Contains some stuff for interacting with SDL
 + ++/
module jengine.core.sdl;

private
{
    import std.file     : text;
    import std.traits   : isFloatingPoint, isNumeric;

    import derelict.sdl2.image, derelict.sdl2.ttf;
}

public
{
	import derelict.sdl2.sdl;
}

/++
 + Thrown when some kind of error with SDL was made.
 + 
 + Parameters:
 +  errorFunc = This function is called to get some kind of error message to accompany the exception.
 + ++/
class _SDLException(alias errorFunc) : Exception
{
    /**
     * Creates a new instance of Exception. The next parameter is used
     * internally and should always be $(D null) when passed by user code.
     * This constructor does not automatically throw the newly-created
     * Exception; the $(D throw) statement should be used for that purpose.
     */
    @trusted this(string msg, string file = __FILE__, size_t line = __LINE__, Throwable next = null)
    {
        super(text(msg, ": ", errorFunc()), file, line, next);
    }
}

/// Ditto
alias SDLException = _SDLException!SDL_GetError;

/// Ditto
alias IMGException = _SDLException!IMG_GetError;

/// Ditto
alias TTFException = _SDLException!TTF_GetError;

/++
 + Loads all of the SDL libraries needed.
 + ++/
void loadSDL()
{
	import std.exception : enforce;
    DerelictSDL2.load();
    DerelictSDL2Image.load();
    DerelictSDL2ttf.load();
    enforce(SDL_Init(SDL_INIT_EVERYTHING) == 0,     new SDLException("Unable to load SDL"));
    enforce(IMG_Init(IMG_INIT_PNG) == IMG_INIT_PNG, new IMGException("Unable to load SDL_IMG with PNG support."));
    enforce(TTF_Init() == 0,                        new TTFException("Unable to load SDL_TTF"));
}

/++
 + A wrapper around SDL_Rectangle, that supports arithmetic operations.
 + ++/
struct Rectangle
{
    private
    {
        SDL_Rect _handle;
    }

    public
    {
        alias handle this;

        /++
         + Creates a new rectangle with the given size and position.
         + 
         + Parameters:
         +  x       = The X position of the rectangle.
         +  y       = The Y position of the rectangle.
         +  width   = The width of the rectangle.
         +  height  = The height of the rectangle.
         + ++/
        @nogc @safe
        this(const(int) x, const(int) y, const(int) width, const(int) height) nothrow
        {
            this(SDL_Rect(x, y, width, height));
        }

        /++
         + Creates a wrapper around the given SDL_Rect.
         + 
         + Parameters:
         +  rect = The SDL_Rect to create a wrapper around.
         + ++/
        @nogc @safe 
        this(const(SDL_Rect) rect) nothrow
        {
            this._handle = rect;
        }

        /++
         + Determines if this rectangle is intersecting with another.
         + 
         + Parameters:
         +  rect = The other rectangle to check for an intersection with.
         + ++/
        @nogc @trusted
        bool intersectsWith(const(Rectangle) rect) nothrow const
        {
            return SDL_HasIntersection(this.handlePointer, rect.handlePointer) == SDL_TRUE;
        }

        /++
         + Determines if a point is inside the rectangle.
         + 
         + Parameters:
         +  point = The point that may or may not be inside the rectangle.
         + ++/
        @trusted @nogc
        bool hasPoint(const(Vector2i) point) nothrow const
        {
            auto sdl = point.asPoint;
            return SDL_PointInRect(&sdl, this.handlePointer) == SDL_TRUE;
        }

        /++
         + Get the inner SDL_Rect that this struct uses.
         + ++/
        @property @nogc @safe
        inout(SDL_Rect) handle() inout nothrow
        {
            return this._handle;
        }

        /++
         + Get a pointer to the inner SDL_Rect that this struct uses.
         + ++/
        @property @nogc @safe
        inout(SDL_Rect)* handlePointer() inout nothrow
        {
            return &this._handle;
        }

        /++
        + Creates a negative version of the Rectangle.
        + ++/
        @nogc @safe
        Rectangle opUnary(string op)() nothrow const
        if(op == "-")
        {
            return Rectangle(-this.x, -this.y, -this.w, -this.h);
        }

        /++
        + Supports binary operations.
        + ++/
        @nogc @safe
        Rectangle opBinary(string op)(Rectangle other) nothrow const
        {
            return mixin("Rectangle(this.x"~op~"other.x, this.y"~op~"other.y, this.w"~op~"other.w, this.h"~op~"other.h)");
        }

        /++
         + Supports Operator assignment. "+="
         + ++/
        @nogc @safe
        void opOpAssign(string op)(Rectangle other) nothrow
        {
            mixin(
                "this.x"~op~"=other.x;"~
                "this.y"~op~"=other.y;"~
                "this.w"~op~"=other.w;"~
                "this.h"~op~"=other.h;"
                );
        }
    }
}
unittest
{
    import std.format : format;

    // I can't test SDL_HasIntersection due to SDL needing to be loaded. So the function will be in one of the bigger tests.
    auto rect1 = const Rectangle(200, 500, -30, -20);
    auto rect2 = -rect1;
    assert(rect2 == Rectangle(-rect1.x, -rect1.y, -rect1.w, -rect1.h), format("%s", rect2));

    rect2 = rect2 + rect1;
    assert(rect2 == Rectangle(0, 0, 0, 0));

    rect2 = ((Rectangle(0, 0, 0, 0) + Rectangle(20, 20, 40, 40)) / (-(-Rectangle(3, 3, 3, 3)) - Rectangle(1, 1, 1, 1)))
            * Rectangle(5, 5, 5, 5);
    assert(rect2 == Rectangle(50, 50, 100, 100), format("%s", rect2));
}

/++
+ A struct that contains 2 of $(B T).
+ 
+ Supports converting it's data into an SDL_Point, when needed.
+ ++/
struct Vector2(T)
if(isNumeric!T)
{
    import std.math : sqrt;

    public
    {
        /// The x value of the vector.
        T x = 0;

        /// The y value of the vector.
        T y = 0;

        static if(isFloatingPoint!T)
        {
            /++
             + Calculates the length of the vector
             + ++/
            @property @nogc @safe
            float length() nothrow const
            {
                return sqrt(this.x * this.x + this.y * this.y);
            }

            /++
             + Returns a normalised version of the vector.
             + ++/
            @property @nogc @safe
            Vector2!T normalised() nothrow const
            {
                auto length = this.length;

                return (length == 0) ? Vector2!T() : Vector2!T(this.x / length, this.y / length);
            }
        }

        /++
         + Converts the vector into an SDL_Point
         + ++/
        @property @nogc @safe
        SDL_Point asPoint() nothrow const
        {
            return SDL_Point(cast(int)this.x, cast(int)this.y);
        }

        /++
         + Converts this vector into another vector.
         + ++/
        @property @nogc @safe
        Vector2!F asVector(F)() nothrow const
        if(isNumeric!F)
        {
            return Vector2!F(cast(F)this.x, cast(F)this.y);
        }

        /++
        + Creates a negative version of the Vector2.
        + ++/
        @nogc @safe
        Vector2!T opUnary(string op)() nothrow const
        if(op == "-")
        {
            return Vector2!T(-this.x, -this.y);
        }
        
        /++
        + Supports binary operations.
        + ++/
        @nogc @safe
        Vector2!T opBinary(string op)(Vector2!T other) nothrow const
        {
            return mixin("Vector2!T(this.x"~op~"other.x, this.y"~op~"other.y)");
        }

        /++
         + Supports Operator assignment. "+="
         + ++/
        @nogc @safe
        void opOpAssign(string op)(Vector2!T other) nothrow
        {
            mixin(
                "this.x"~op~"=other.x;"~
                "this.y"~op~"=other.y;"
                );
        }
    }
}
unittest
{
    import std.format : format;

    auto point1 = const Vector2i(200, -500);
    auto point2 = -point1;
    assert(point2 == Vector2i(-point1.x, -point1.y), format("%s", point2));
    
    point2 = point2 + point1;
    assert(point2 == Vector2i(0, 0));
    
    point2 = ((Vector2i(0, 0) + Vector2i(20, 40)) / (-(-Vector2i(3, 3)) - Vector2i(1, 1)))
            * Vector2i(5, 5);
    assert(point2 == Vector2i(50, 100), format("%s", point2));

    assert(point2.asPoint == SDL_Point(50, 100));
}

alias Vector2f = Vector2!float;
alias Vector2i = Vector2!int;
alias Vector2u = Vector2!uint;

/++
 + A struct representing a colour.
 + ++/
struct Colour
{
    public
    {
        ubyte r, g, b;
        ubyte a = 255;

        /++
        + Creates a negative version of the Colour.
        + The alpha value of the colour is not touched.
        + ++/
        @nogc @safe
        Colour opUnary(string op)() nothrow const
        if(op == "-")
        {
            return Colour(-this.r, -this.g, -this.b, this.a);
        }
        
        /++
        + Supports binary operations.
        + ++/
        @nogc @safe
        Colour opBinary(string op)(Colour other) nothrow const
        {
            return mixin("Colour(cast(ubyte)(this.r"~op~"other.r), cast(ubyte)(this.g"~op~"other.g), cast(ubyte)(this.b"~op~"other.b), this.a)");
        }

        /++
         + Supports Operator assignment. "+="
         + ++/
        @nogc @safe
        void opOpAssign(string op)(Colour other) nothrow
        {
            mixin(
                "this.r"~op~"=other.r;"~
                "this.g"~op~"=other.g;"~
                "this.b"~op~"=other.b;"
                );
        }

        import derelict.sdl2.ttf : SDL_Color;
        @property @nogc @safe
        SDL_Color asSDLColour() nothrow
        {
            return SDL_Color(this.r, this.g, this.b, this.a);
        }
    }
}

/++
 + Describes a circle.
 + ++/
struct Circle
{
    public
    {
        /++
         + The radius of the circle.
         + ++/
        float       radius;
        
        /++
         + The position of the circle.
         + ++/
        Vector2f    position;
        
        /++
         + Determines if this Circle and another Circle are colliding.
         + 
         + Parameters:
         +  circle = The other Circle.
         + 
         + Returns:
         +  $(D true) if $(B circle) and this are colliding.
         + ++/
        @nogc @safe
        bool collides(const(Circle) circle) nothrow pure const
        {
            return (this.radius + circle.radius)^^2 > (this.position.x - circle.position.x)^^2 + (this.position.y - circle.position.y)^^2;
        }

        /++
         + Determines if this Circle contains a point.
         + 
         + Parameters:
         +  point = The point to find.
         + 
         + Returns:
         +  $(D true) if $(B point) is inside the circle.
         + ++/
        @nogc @safe
        bool hasPoint(const(Vector2f) point) nothrow pure const
        {
            return (point.x - this.position.x)^^2 + (point.y - this.position.y)^^2 <= this.radius^^2;
        }

        /++
         + Calculates the center point of the circle.
         + ++/
        @property @nogc @safe
        Vector2f centre() nothrow pure const
        {
            return Vector2f(this.position.x + this.radius, this.position.y + this.radius);
        }
    }
}
unittest
{
    assert( Circle(5,  Vector2f()).collides(Circle(2, Vector2f(6, 1))));
    assert(!Circle(20, Vector2f()).collides(Circle(8, Vector2f(50, 0))));
    assert( Circle(20, Vector2f()).hasPoint(Vector2f(5, 5)));
    assert(!Circle(20, Vector2f()).hasPoint(Vector2f(200, 0)));
}