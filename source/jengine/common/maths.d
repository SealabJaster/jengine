/// Publicly imports all of the maths code from GFM.
module jengine.common.maths; // Note: I'm doing this mostly because I want things named "Box2f" instead of "box2f"

public
{
    import gfm.math;
}

alias Box2f = box2f;
alias Box2d = box2d;

alias Mattix2f = mat2f;
alias Matrix2d = mat2d;
alias Matrix3f = mat3f;
alias Matrix3d = mat3d;

alias QuaternionF = quatf;
alias QuaternionD = quatd;

alias Vector2f = vec2f;
alias Vector2d = vec2d;
alias Vector2i = vec2i;
alias Vector3f = vec3f;
alias Vector4f = vec4f;

alias Line2f = seg2f;
alias Line2d = seg2d;
alias Line2i = seg2i;

alias Triangle2f = triangle2f;
alias Triangle2d = triangle2d;

alias Sphere2f = sphere2f;
alias Sphere2d = sphere2d;

alias Ray2f = ray2f;
alias Ray2d = ray2d;