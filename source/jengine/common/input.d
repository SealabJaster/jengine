///
module jengine.common.input;

private import derelict.glfw3.glfw3 : GLFW_PRESS, GLFW_REPEAT, GLFW_RELEASE, 
                                      GLFW_MOD_SHIFT, GLFW_MOD_CONTROL, GLFW_MOD_ALT, GLFW_MOD_SUPER;

/// Defines the state of some kind of input.
enum InputState : int
{
    /// The input is being/has been pressed.
    press = GLFW_PRESS,

    /// The input has been repeated (such as holding down a key on the keyboard).
    repeat = GLFW_REPEAT,

    /// The input has been released.
    release = GLFW_RELEASE
}

/++
 + Defines modifiers to key input.
 +
 + The values of this enum can be ORed together.
 + ++/
enum KeyModifier : int
{
    /// The shift key was pressed.
    shift = GLFW_MOD_SHIFT,

    /// The control key was pressed.
    control = GLFW_MOD_CONTROL,

    /// The alt key was pressed.
    alt = GLFW_MOD_ALT,

    /// A super key was pressed.
    super_ = GLFW_MOD_SUPER
}