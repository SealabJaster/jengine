﻿module jengine.tests.t02;

version(unittest)
{
    import std.stdio, std.experimental.logger;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.gamestate;
    import jengine.graphics.window;

    void t02()
    {
        Window.create("Test 02", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();

        auto app = Allocators.GameState.make!Game;

        Window.startGameLoop(app);
    }

    class Game : GameState
    {
        private
        {
            struct Output
            {
                void put(string s)
                {
                    info(s);
                }
            }

            Timer t;
            FPSReporter!Output fps;
        }

        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                writeln("On init");
                this.t.restart();

                this.fps = theAllocator.make!(FPSReporter!Output)(Output());
                this.fps.onInit();

                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                if(t.asMilliseconds >= 1000)
                {
                    writeln(gameTime);
                    t.restart();
                }

                auto text = InputManager.textBuffer;
                if(text.length != 0)
                    info(text);

                if(InputManager.isKeyPressed(SDL_SCANCODE_Q))
                    writeln("Quack");

                this.fps.onUpdate(gameTime);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                if(!InputManager.isKeyDown(SDL_SCANCODE_TAB))
                    writeln("Event = ", event.type);

                return true;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                writeln("On uninit");
                theAllocator.dispose(this.fps);

                super.onUninit();
            }
        }
    }
}