﻿module jengine.tests.t06;

version(unittest)
{
    import std.stdio, std.experimental.logger;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.world, jengine.core.gamestate;
    import jengine.graphics.window, jengine.graphics.renderer, jengine.graphics.texture;
    
    void t06()
    {
        Window.create("Test 06", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;

        PhysicalWorld.setup();
        scope(exit) PhysicalWorld.destroy();

        Window.startGameLoop(app);
    }

    class Test : PhysicalComponent
    {
        bool player = false;
        this(bool player)
        {
            this.player = player;
        }

        public override
        {
            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                this.sprite = Sprite.make(Texture.fromFile("Data/Tahn.png\0"), (this.player) ? Vector2f(0, 720/2) : Vector2f(860/2, 720/2));

                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                if(this.player)
                    this.sprite.position += Vector2f(50.0f * gameTime, 0);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return false;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                // Bit of a hacky way, but meh. (You can't pass sprite.texture directly, but because Texture is a reference-type you can just store a variable of it somewhere)
                auto texture = this.sprite.texture;
                Texture.dispose(texture);
                super.onUninit();
            }

            /++
             + Called whenever the physical component overlaps with another.
             + 
             + Parameters:
             +  other = The other entity.
             + ++/
            void onHitEntity(PhysicalComponent other)
            {
                if(!this.player)
                    PhysicalWorld.remove(this);
            }
        }
    }

    class Game : GameState
    {
        private
        {
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                PhysicalWorld.add(theAllocator.make!Test(false));
                PhysicalWorld.add(theAllocator.make!Test(true));

                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                PhysicalWorld.onUpdate(gameTime);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return PhysicalWorld.onEvent(event, handled);
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                PhysicalWorld.destroy();
                super.onUninit();
            }
        }
    }
}