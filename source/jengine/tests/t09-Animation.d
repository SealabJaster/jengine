﻿module jengine.tests.t09;

version(unittest)
{
    import std.stdio, std.experimental.logger;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.world, jengine.core.gamestate;
    import jengine.graphics.window, jengine.graphics.renderer, jengine.graphics.texture, jengine.graphics.camera;
    
    void t09()
    {
        Window.create("Test 09", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;
        
        PhysicalWorld.setup();
        scope(exit) PhysicalWorld.destroy();
        
        Window.startGameLoop(app);
    }
    
    private Texture Tahn;

    class Game : GameState
    {
        private
        {
            Animation   _looping;
            Animation   _notLooping;
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}
            
            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                Tahn                = Texture.fromFile("Data/TahnAni.png");
                this._looping       = Animation.make(Tahn, AnimationInfo(32, 32, 0.75f, true));
                this._notLooping    = Animation.make(Tahn, AnimationInfo(32, 32, 2.0f, false));

                this._notLooping.sprite.position += Vector2f(38, 0);

                this._looping.start();
                this._notLooping.start();
                this._notLooping.nextColumn();
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                this._looping.onUpdate();
                this._notLooping.onUpdate();

                Renderer.drawAnimation(this._looping);
                Renderer.drawAnimation(this._notLooping);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return handled;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                Texture.dispose(Tahn);
                Animation.dispose(this._looping);
                Animation.dispose(this._notLooping);
                super.onUninit();
            }
        }
    }
}