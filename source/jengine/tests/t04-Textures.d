﻿module jengine.tests.t04;

version(unittest)
{
    import std.stdio;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.gamestate;
    import jengine.graphics.window, jengine.graphics.renderer, jengine.graphics.texture;
    
    void t04()
    {
        Window.create("Test 04", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;
        
        Window.startGameLoop(app);
    }
    
    class Game : GameState
    {
        private
        {
            Texture tahn;
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                tahn = Texture.fromFile("Data/Tahn.png\0");
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            float angle = 0;
            void onUpdate(ref float gameTime)
            {
                angle += gameTime;

                tahn.colour = tahn.colour + Colour(5, 0, 0);
                Renderer.drawTexture(tahn, Rectangle(), Rectangle(), angle);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return false;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                Texture.dispose(tahn);
                super.onUninit();
            }
        }
    }
}