﻿/++
 + Contains smart pointers to make life management a bit less evil.
 + ++/
module jaster.smartptr; // I've honestly been struggling about thinking how to keep things alive long enough without
                        // constructs like these e_e (in code that makes use of std.allocators)

private
{
    import std.experimental.allocator, std.experimental.allocator.mallocator;
    import std.typecons         : Flag, Yes, No;
    import std.traits           : isInstanceOf;

    import jaster.containers    : _AllocatorBoilerplate;
    import jaster.traits        : mightUseGCMemory;
}

/++
 + An implementation of a shared pointer.
 + 
 + Description:
 +  $(P The data that a shared pointer points to is accompanied with a reference counter (which is just a number).)
 +  $(P When the reference counter reaches 0, the data the pointer points to is destroyed. Incrementing the reference
 +      counter is done manually, with `addRef`. Decrementing the counter is also done manually, with `removeRef`)
 +  $(P It is up to the programmer to correctly call addRef and removeRef, otherwise crashes are most likely to occur.)
 + 
 + Params:
 +  T       = The type of data this pointer points to.
 +  Alloc   = The allocator to use to allocate the pointer's storage (if needed).
 +  useGC   = Please see the documentation for `jaster.containers.DynamicArray` for explanation of this.
 + ++/
struct SharedPointer(T, Alloc = Mallocator, Flag!"useGC" useGC = cast(Flag!"useGC")mightUseGCMemory!T)
{
    import std.traits : isCopyable;

    alias data this;
    mixin _AllocatorBoilerplate!Alloc;

    /// An alias to the `T` template paramter.
    alias DataT = T;

    private
    {
        struct Shared
        {
            T     data;
            uint  refCount;
        }

        Shared* _data;
    }

    public
    {
        /++
         + Creates a new shared pointer using the given value.
         + 
         + Notes:
         +  $(P This constructor will function identically to whichever overload of `SharedPointer.set` applies
         +      to `T`. So please refer to it's documentation for specifics.)
         +  $(P If `Alloc` needs to be provided, then please either use `SharedPointer.alloc` or one of the other constructors
         +      which takes `Alloc` as a parameter.)
         + 
         + Params:
         +  value = The initial value of the pointer.
         + ++/
        this(OT : T)(auto ref OT value)
        {
            this.set(value);
        }
        ///
        unittest
        {
            // See the examples for `SharedPointer.set` for specifics.
            // Copyable types
            auto pointer = SharedPointer!string("Hello world!");
            assert(!pointer.isNull && pointer.refCount == 1);
            assert( pointer == "Hello world!");

            // Non-copyable types
            alias Type = NonCopyableWithNumber;
            Type data;
            data.number = 200;

            auto ncPointer = SharedPointer!Type(data);
            assert(data             == Type.init);
            assert(data.number      == 0);
            assert(ncPointer.number == 200);

            pointer.removeRef;
            ncPointer.removeRef;
        }

        static if(!_allocIsStatic)
        {
            /++
             + Creates a new shared pointer using the given allocator and value.
             + 
             + Notes:
             +  Please see the documentation for the constructor that simply takes a value for more information.
             + 
             + Params:
             +  alloc = The `Alloc` instance to use.
             +  value = The initial value of the pointer.
             + ++/
            this(OT : T)(Alloc alloc, auto ref OT value)
            {
                this.alloc = alloc;
                this(value);
            }
            ///
            unittest
            {
                alias AllocType = CAllocatorImpl!(shared Mallocator);
                alias PointerT  = SharedPointer!(string, AllocType);
                auto allocator  = allocatorObject(Mallocator.instance);
                auto pointer    = PointerT(allocator, "Hello world!");

                assert(pointer  == "Hello world!");
                pointer.removeRef;
            }
        }

        /++
         + Increments the pointer's reference count.
         + 
         + Assertions:
         +  The pointer's `isNull` must return `false`.
         + 
         + See_Also:
         +  `removeRef`
         + ++/
        @nogc
        void addRef() nothrow
        {
            assert(!this.isNull, "The pointer hasn't been set to a value.");
            this._data.refCount += 1;
        }
        ///
        unittest
        {
            SharedPointer!uint pointer;
            assert(pointer.isNull && pointer.refCount == 0);

            pointer.set(300);
            assert(!pointer.isNull);
            assert( pointer.refCount == 1);

            pointer.addRef();
            assert(pointer.refCount == 2);

            // Make sure to remove the references, otherwise you'll have a memory leak.
            foreach(i; 0..2)
                pointer.removeRef();
            assert(pointer.isNull && pointer.refCount == 0);
        }

        /++
         + Decrements the pointer's reference count.
         + 
         + Description:
         +  When the reference count reaches 0, then the data this pointer points to will be destroyed, and then
         +  deallocated.
         + 
         + Allocation:
         +  Deallocates it's data when the reference count reaches 0.
         + 
         + Notes:
         +  Any other copy of this pointer should have their `free` function be called, should they continue to be used.
         +  Usage of these older copies will result in an access violation otherwise, as it will be trying to use
         +  deallocated memory.
         + 
         + See_Also:
         +  `addRef`
         + ++/
        void removeRef()()
        {
            assert(!this.isNull, "The pointer hasn't been set to a value.");
            this._data.refCount -= 1;

            if(this._data.refCount == 0)
            {
                static if(useGC)
                {
                    import core.memory : GC;
                    GC.removeRange(this._data);
                }

                destroy(this._data.data);
                this.alloc.dispose(this._data);
                this._data = null;
                assert(this.isNull);
            }
        }
        ///
        unittest
        {
            SharedPointer!string pointer;
            pointer.set("Daniel is soupy");
            pointer.addRef();

            assert(pointer.refCount == 2);
            pointer.removeRef();
            assert(pointer.refCount == 1);
            pointer.removeRef(); // This will bring the refCount to 0, causing the data to be deallocated.

            assert(pointer.isNull && pointer.refCount == 0);
        }

        /++
         + Sets the data of the pointer (and any other copies of this pointer).
         + 
         + Notes:
         +  This function is probably suitable for most types, it will simply copy `value` into the internal pointer,
         +  granted, the type `OT` can implicitly convert to `T`.
         + 
         + Allocation:
         +  Follows the rules for `ensureNotNull`.
         + 
         + Params:
         +  value = The value to give the pointer.
         + ++/
        void set(OT : T)(auto ref OT value)
        if(isCopyable!T)
        {
            this.ensureNotNull();
            this._data.data = value;
        }
        ///
        unittest
        {
            SharedPointer!uint pointer;

            assert(pointer.isNull);
            pointer.set(300);

            assert(!pointer.isNull);
            assert( pointer == 300);

            // Bit of an annoying syntax.
            // Because the pointer has an alias this to "data", and "data" returns a reference
            // Stuff like below is fine. However, you can only do this after a call to anything that calls "ensureNotNull"
            pointer = 600;
            assert(pointer == 600);
            pointer.removeRef;
        }

        /++
         + Sets the data of the pointer (and any other copies of this pointer).
         + 
         + Notes:
         +  This function is used for non-copyable types (types that use `@disable this(this)`).
         + 
         + Allocation:
         +  Follows the rules for `ensureNotNull`.
         + 
         + Params:
         +  value = The value to give the pointer. 
         +          $(B Note that this value will be overwritten with `T.init`)
         + ++/
        void set()(ref T value)
        if(!isCopyable!T)
        {
            import std.algorithm : move;
            import std.conv      : emplace;

            this.ensureNotNull();
            move(value, this._data.data);
            emplace!T(&value);
        }
        ///
        unittest
        {
            // I couldn't actually put this struct into the unittest, because it causes an infinite loop in the compiler o_o
            alias Type = NonCopyableWithNumber;
            SharedPointer!Type pointer;

            Type thing;
            thing.number = 200;

            pointer.set(thing); // "thing" is now overwritten
            assert(thing          == Type.init);
            assert(thing.number   == 0);
            assert(pointer.number == 200);
            pointer.removeRef;
        }

        /++
         + Ensures that the pointer refers to data.
         + 
         + Allocation:
         +  If the pointer `isNull`, then this function will allocate a small struct, simply containing a `T` and a `uint`.
         + 
         + Notes:
         +  If the pointer had to allocate it's data, then it's ref count will default to 1.
         + ++/
        void ensureNotNull()()
        {
            if(this.isNull)
            {
                import std.conv : emplace;

                // If T is not copyable, then std.allocators.make will fail, so we need to allocate stuff manually.
                this._data          = (cast(Shared[])this.alloc.allocate(Shared.sizeof)).ptr;
                this._data.refCount = 1;
                emplace!T(&this._data.data); // I *think* this is how we can set it to an intial value ;-;

                static if(useGC)
                {
                    import core.memory;
                    GC.addRange(this._data,
                                Shared.sizeof,
                                typeid(Shared));
                }
            }

            assert(!this.isNull, "D: Still null. Please report this.");
        }
        ///
        unittest
        {
            SharedPointer!T pointer;
            assert(pointer.isNull);
            pointer.ensureNotNull();
            assert(!pointer.isNull);

            pointer.removeRef();
        }

        /++
         + Frees this pointer from it's data.
         + 
         + Allocation:
         +  Follows the rules for `removeRef`.
         + 
         + Notes:
         +  If this pointer does not point to any data (`isNull`) then this is function does nothing.
         + 
         + Params:
         +  decRef = If `Yes.removeRef`, then `removeRef` is called.
         +           If `No.removeRef`, then the pointer simply stops pointing to anything.
         + 
         + See_Also:
         +  `removeRef`
         + ++/
        void free(Flag!"removeRef" decRef = Yes.removeRef)()
        {
            if(this.isNull)
                return;

            static if(decRef)
                this.removeRef();

            this._data = null;
            assert(this.isNull);
        }
        ///
        unittest
        {
            SharedPointer!uint pointer;
            pointer.set(200);

            auto pointer2 = pointer; // Copying doesn't add a reference, so refCount is still at 1.
            assert(pointer  == 200);
            assert(pointer2 == 200);

            pointer.free!(Yes.removeRef); // This will remove a reference, making the refCount 0. Causing the data to be deallocated.

            // However, pointer2 doesn't know about this:
            assert( pointer.isNull);
            assert(!pointer2.isNull); // <----

            // So if we ever wanted to use pointer2 again, we'd first have to free it
            pointer2.free!(No.removeRef);
            assert(pointer2.isNull); // It can now be used again safely.

            // If we didn't free it, then most of the functions would either fail an assert, or trigger an access violation.
            // pointer2.data; // <--- Crash
        }

        /++
         + Determines if this pointer refers to some data.
         + 
         + Returns:
         +  `true` if this pointer points to some data.
         +  `false` if this pointer points to no data.
         + 
         + See_Also:
         +  `ensureNotNull`
         + ++/
        @property @safe @nogc
        bool isNull() nothrow const
        {
            return (this._data is null);
        }
        ///
        unittest
        {
            SharedPointer!T pointer;
            assert(pointer.isNull);
        }

        /++
         + Assertions:
         +  The pointer's `isNull` function should return `false`.
         + 
         + Returns:
         +  The data that this pointer is pointing to.
         + ++/
        @property @safe @nogc
        ref inout(T) data() nothrow inout
        {
            assert(!this.isNull, "The pointer hasn't been set to a value.");
            return this._data.data;
        }
        ///
        unittest
        {
            SharedPointer!uint pointer;
            pointer.ensureNotNull();

            pointer = 369;
            assert(pointer == 369);

            pointer.removeRef();
        }

        /++
         + Returns:
         +  If the pointer points to nothing (`isNull` is true), then 0 is returned.
         +  Otherwise, the ref count of the pointer's data is returned.
         + ++/
        @property @safe @nogc
        uint refCount() nothrow const
        out(count)
        {
            if(count == 0)
                assert(this.isNull);
        }
        body
        {
            return (this.isNull) ? 0 : this._data.refCount;
        }
        ///
        unittest
        {
            SharedPointer!T pointer;
            pointer.ensureNotNull();
            assert(pointer.refCount == 1);
            pointer.addRef;
            assert(pointer.refCount == 2);
            pointer.removeRef();
            assert(pointer.refCount == 1);
            pointer.removeRef();
            assert(pointer.isNull && pointer.refCount == 0);
        }
    }
}

// Test to make sure non-copyable stuff will work.
unittest
{
    import jaster.containers : DynamicArray;
    alias ArrayT = DynamicArray!int;

    SharedPointer!ArrayT array;
    array.ensureNotNull();
    array ~= [20, 40, 60, 80];
    assert(array.range == [20, 40, 60, 80]);

    ArrayT array2;
    array2 ~= [100, 120, 140, 160];
    array.set(array2); // For non-copyable types, the original is overwritten with T.init

    assert(array2      == ArrayT.init);
    assert(array.range == [100, 120, 140, 160]);

    array.removeRef();
}

version(unittest)
{
    struct TestSPStruct {}
    class  TestSPClass  {}

    struct NonCopyableWithNumber
    {
        int number;
        @disable this(this) {}
    }

    alias TestSPI  = SharedPointer!int;
    alias TestSPS  = SharedPointer!string;
    alias TestSPSt = SharedPointer!TestSPStruct;
    alias TestSPC  = SharedPointer!TestSPClass;
    alias TestNSA  = SharedPointer!(int, CAllocatorImpl!Mallocator);
}

/++
 + Determines if a type is a `SharedPointer`.
 + 
 + Params:
 +  T = The type to check.
 + 
 + Returns:
 +  `true` if `T` is some form of `SharedPointer`.
 +  `false` otherwise.
 + ++/
enum isSharedPointer(T) = isInstanceOf!(SharedPointer, T);
///
unittest
{
    import std.experimental.allocator.gc_allocator;

    static assert( isSharedPointer!(SharedPointer!int));
    static assert( isSharedPointer!(SharedPointer!(string, GCAllocator)));
    static assert(!isSharedPointer!int);
}

/++
 + Determines if a type is a `SharedPointer`, and it holds a certain type of data.
 + 
 + Params:
 +  P = The type to test as a shared pointer.
 +  T = The type to check that `P` stores.
 + 
 + Returns:
 +  `true` if `P` is a `SharedPointer` and stores values of `T`.
 +  `false` otherwise.
 + ++/
enum isSharedPointerOf(P, T) = (isSharedPointer!P && is(P.DataT == T));
///
unittest
{
    static assert( isSharedPointerOf!(SharedPointer!int, int));
    static assert(!isSharedPointerOf!(SharedPointer!int, string));
}

/++
 + A wrapper around `SharedPointer` that will increment the reference count anytime the pointer is copied, and
 + decrement the reference count anytime a pointer goes out of scope.
 + 
 + $(I See `SharedPointer` for most documentation.)
 + 
 + Notes:
 +  $(P This type is `alias this`ed to a `SharedPointer`, so should function as such.)
 +  $(P This struct does not (currently) provide a constructor to pass an instance of `Alloc` to it's `SharedPointer`,
 +      so as a reminder, the allocator can be set using `autoShared.alloc = Blah;`)
 + ++/
struct AutoSharedPointer(T, Alloc = Mallocator, Flag!"useGC" useGC = cast(Flag!"useGC")mightUseGCMemory!T)
{
    alias pointer this;
    alias SharedPointerT = SharedPointer!(T, Alloc, useGC);

    private
    {
        SharedPointerT _pointer;
    }

    public
    {
        /// Increments the referece count.
        @nogc @trusted
        this(this) nothrow
        {
            if(this._pointer.isNull)
                return;

            this._pointer.addRef();
        }

        /++
         + Sets the value of the pointer.
         + 
         + Please see the documentation for `SharedPointer.this` for full information.
         + 
         + Params:
         +  value = The value to give the pointer.
         + ++/
        this(OT : T)(OT value)
        {
            this._pointer.set(value);
        }

        /// Decrements the reference count.
        ~this()
        {
            if(this._pointer.isNull)
                return;

            this._pointer.removeRef();
        }

        /++
         + Returns:
         +  A reference to the `SharedPointer` that is being wrapped around.
         + ++/
        @property @safe @nogc
        ref inout(SharedPointerT) pointer() nothrow inout
        {
            return this._pointer;
        }
    }
}
///
unittest
{
    auto pointer = AutoSharedPointer!int(200);
    assert(!pointer.isNull);
    assert(pointer.refCount == 1);
    assert(pointer          == 200);

    {
        auto otherPointer = pointer;
        otherPointer.set(500);
        assert(!otherPointer.isNull);
        assert(otherPointer.refCount == 2);
        assert(otherPointer          == 500);
        assert(pointer               == 500);
    }

    assert(pointer.refCount == 1);
}